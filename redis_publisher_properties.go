package golibrmsg

import (
	"gitlab.com/golibs-starter/golib/config"
)

// RedisPublisherProperties represent swagger configuration properties
type RedisPublisherProperties struct {
	EventMappings map[string]*RedisPublisherDataProperties
}

// RedisPublisherDataProperties represent swagger configuration properties
type RedisPublisherDataProperties struct {
	Channel string
}

// NewRedisPublisherProperties create a new RedisPublisherProperties instance
func NewRedisPublisherProperties(loader config.Loader) (*RedisPublisherProperties, error) {
	props := RedisPublisherProperties{}
	err := loader.Bind(&props)
	return &props, err
}

// Prefix return yaml prefix of configuration
func (t *RedisPublisherProperties) Prefix() string {
	return "app.redis.publisher"
}
