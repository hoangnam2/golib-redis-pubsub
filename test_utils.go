package golibrmsg

import (
	"encoding/json"
	"fmt"
	coreUtils "gitlab.com/golibs-starter/golib/utils"
	"go.uber.org/fx"
	"strings"
)

var subscriberMap map[string]RedisSubscriberHandler

// HandleMessage handle message
func HandleMessage(subscriberName string, message []byte) {
	subscriber, ok := subscriberMap[strings.ToLower(subscriberName)]
	if !ok {
		panic(fmt.Sprintf("subscriber with name %v not found", subscriberName))
	}
	var redisMessage *RedisMessage
	err := json.Unmarshal(message, &redisMessage)
	if err != nil {
		panic(err)
	}
	subscriber.HandlerFunc(redisMessage)
}

// EnableRedisSubscriberTestUtil ...
func EnableRedisSubscriberTestUtil() fx.Option {
	return fx.Invoke(fx.Annotate(func(subscribers []RedisSubscriberHandler) {
		subscriberMap = make(map[string]RedisSubscriberHandler, 0)
		for _, subscriber := range subscribers {
			subscriberMap[strings.ToLower(coreUtils.GetStructShortName(subscriber))] = subscriber
		}
	}, fx.ParamTags(`group:"redis_consumer_handler"`)))
}
