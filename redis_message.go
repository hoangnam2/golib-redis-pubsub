package golibrmsg

import (
	"github.com/hashicorp/go-uuid"
	"time"
)

// RedisMessage ...
type RedisMessage struct {
	ID          string      `json:"id,omitempty"`
	Event       string      `json:"event,omitempty"`
	ServiceCode string      `json:"service_code,omitempty"`
	Timestamp   int64       `json:"timestamp,omitempty"`
	Payload     interface{} `json:"payload,omitempty"`
}

// NewRedisMessage ...
func NewRedisMessage(event, serviceCode string, payload interface{}) (*RedisMessage, error) {
	id, _ := uuid.GenerateUUID()
	return &RedisMessage{
		ID:          id,
		Event:       event,
		ServiceCode: serviceCode,
		Timestamp:   time.Now().UnixMilli(),
		Payload:     payload,
	}, nil
}

func (r *RedisMessage) Decode(result interface{}) error {
	return decode(r.Payload, result)
}
