package golibrmsg

import (
	"context"
	"encoding/json"
	"github.com/go-redis/redis/v8"
	"gitlab.com/golibs-starter/golib/web/log"
	"strings"
)

// RedisPublisher ...
type RedisPublisher struct {
	client *redis.Client
	props  *RedisPublisherProperties
}

// NewRedisPublisher ...
func NewRedisPublisher(
	client *redis.Client,
	props *RedisPublisherProperties,
) *RedisPublisher {
	return &RedisPublisher{
		client: client,
		props:  props,
	}
}

// Publish ...
func (p *RedisPublisher) Publish(ctx context.Context, event string, payload interface{}) {
	config, ok := p.props.EventMappings[strings.ToLower(event)]
	if !ok {
		log.Errorf("redis publisher error: event %s not configured", event)
		return
	}
	go func(ctx context.Context, config *RedisPublisherDataProperties) {
		message, err := NewRedisMessage(event, "Market Trade Worker", payload)
		if err != nil {
			log.Errorf("redis publisher error: failed to create message %+v error: %v", message, err)
			return
		}
		marshaledMessage, err := json.Marshal(message)
		if err != nil {
			log.Errorf("redis publisher error: failed to marshal message %+v error: %v", message, err)
			return
		}
		if err := p.client.Publish(ctx, config.Channel, marshaledMessage).Err(); err != nil {
			log.Errorf("redis publisher error: failed to publish message %s to channel %s: %v", string(marshaledMessage), config.Channel, err)
			return
		}
		log.Debugf("published message %s to channel %s", string(marshaledMessage), config.Channel)
	}(ctx, config)
}
