package golibrmsg

import (
	"gitlab.com/golibs-starter/golib/config"
)

// RedisSubscriberProperties represent swagger configuration properties
type RedisSubscriberProperties struct {
	HandlerMappings map[string]*RedisSubscriberDataProperties
}

// RedisSubscriberDataProperties represent swagger configuration properties
type RedisSubscriberDataProperties struct {
	Channels []string
	Enable   bool
}

// NewRedisSubscriberProperties create a new RedisSubscriberProperties instance
func NewRedisSubscriberProperties(loader config.Loader) (*RedisSubscriberProperties, error) {
	props := RedisSubscriberProperties{}
	err := loader.Bind(&props)
	return &props, err
}

// Prefix return yaml prefix of configuration
func (t *RedisSubscriberProperties) Prefix() string {
	return "app.redis.subscriber"
}
