package golibrmsg

import (
	"context"
	"encoding/json"
	"github.com/go-redis/redis/v8"
	coreUtils "gitlab.com/golibs-starter/golib/utils"
	"gitlab.com/golibs-starter/golib/web/log"
	"strings"
)

// RedisSubscriber ...
type RedisSubscriber struct {
	client      *redis.Client
	name        string
	channels    []string
	handler     RedisSubscriberHandler
	redisPubSub *redis.PubSub
	stopCh      chan bool
}

// NewRedisSubscriber ...
func NewRedisSubscriber(
	client *redis.Client,
	props *RedisSubscriberDataProperties,
	handler RedisSubscriberHandler,
) *RedisSubscriber {

	channels := make([]string, 0)
	for _, channel := range props.Channels {
		channels = append(channels, strings.TrimSpace(channel))
	}

	return &RedisSubscriber{
		client:   client,
		name:     coreUtils.GetStructShortName(handler),
		channels: channels,
		handler:  handler,
	}
}

// Start ...
func (c *RedisSubscriber) Start(ctx context.Context) {
	log.Infof("Subscriber [%s] with channels [%v] is running", c.name, c.channels)
	defer log.Infof("Subscriber [%s] with channels [%v] is closed", c.name, c.channels)

	c.redisPubSub = c.client.Subscribe(ctx, c.channels...)
	msCh := c.redisPubSub.Channel()
	for {
		select {
		case message := <-msCh:
			var msg RedisMessage
			if err := json.Unmarshal([]byte(message.Payload), &msg); err != nil {
				log.Errorf("redis subscriber: failed to parse message: %v", err)
				continue
			}
			c.handler.HandlerFunc(&msg)
		case <-c.stopCh:
			return
		}
	}
}

// Stop ..
func (c *RedisSubscriber) Stop() {
	log.Infof("Subscriber [%s] with channels [%v] is stopping", c.name, c.channels)
	defer log.Infof("Subscriber [%s] with channels [%v] is stopped", c.name, c.channels)

	c.stopCh <- true
	c.handler.Close()
	if err := c.redisPubSub.Close(); err != nil {
		log.Errorf("Subscriber [%s] with channels [%v] failed to close. Error %v", c.name, c.channels, err)
	}
}
