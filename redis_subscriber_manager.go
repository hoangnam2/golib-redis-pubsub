package golibrmsg

import (
	"context"
	"github.com/go-redis/redis/v8"
	"github.com/pkg/errors"
	coreUtils "gitlab.com/golibs-starter/golib/utils"
	"gitlab.com/golibs-starter/golib/web/log"
	"strings"
	"sync"
)

// RedisSubscriberManager ...
type RedisSubscriberManager struct {
	subscribers map[string]*RedisSubscriber
}

// NewRedisSubscriberManager ...
func NewRedisSubscriberManager(
	handlers []RedisSubscriberHandler,
	client *redis.Client,
	subscriberProps *RedisSubscriberProperties,
) (*RedisSubscriberManager, error) {
	if len(subscriberProps.HandlerMappings) < 1 {
		return nil, errors.New("redis subscriber: missing handler mapping")
	}

	handlerMap := make(map[string]RedisSubscriberHandler)
	for _, handler := range handlers {
		handlerMap[strings.ToLower(coreUtils.GetStructShortName(handler))] = handler
	}

	subscribers := make(map[string]*RedisSubscriber)
	for key, config := range subscriberProps.HandlerMappings {
		if !config.Enable {
			log.Debugf("redis subscriber key [%s] is not enabled", key)
			continue
		}

		key = strings.ToLower(strings.TrimSpace(key))
		handler, exists := handlerMap[key]
		if !exists {
			log.Debugf("redis subscriber key [%s] is not exists in handler list", key)
			continue
		}

		subscribers[key] = NewRedisSubscriber(client, config, handler)
	}

	return &RedisSubscriberManager{
		subscribers: subscribers,
	}, nil
}

// Start ...
func (m *RedisSubscriberManager) Start(ctx context.Context) {
	wg := new(sync.WaitGroup)
	wg.Add(len(m.subscribers))
	for _, subscriber := range m.subscribers {
		go func(subscriber *RedisSubscriber) {
			defer wg.Done()
			subscriber.Start(ctx)
		}(subscriber)
	}
	wg.Wait()
}

// Stop ...
func (m *RedisSubscriberManager) Stop() {
	wg := new(sync.WaitGroup)
	wg.Add(len(m.subscribers))
	for _, subscriber := range m.subscribers {
		go func(subscriber *RedisSubscriber) {
			defer wg.Done()
			subscriber.Stop()
		}(subscriber)
	}
	wg.Wait()
}
