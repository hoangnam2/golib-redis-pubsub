package golibrmsg

import (
	"errors"
	"fmt"
	"github.com/mitchellh/mapstructure"
	"github.com/shopspring/decimal"
	"math/big"
	"reflect"
	"time"
)

func toTimeHookFunc() mapstructure.DecodeHookFunc {
	return func(
		f reflect.Type,
		t reflect.Type,
		data interface{}) (interface{}, error) {
		if t != reflect.TypeOf(time.Time{}) {
			return data, nil
		}

		switch f.Kind() {
		case reflect.String:
			return time.Parse(time.RFC3339Nano, data.(string))
		case reflect.Float64:
			return time.Unix(0, int64(data.(float64))*int64(time.Millisecond)), nil
		case reflect.Int64:
			return time.Unix(0, data.(int64)*int64(time.Millisecond)), nil
		default:
			return data, nil
		}
	}
}

func toDecimalHookFunc() mapstructure.DecodeHookFunc {
	return func(
		f reflect.Type,
		t reflect.Type,
		data interface{}) (interface{}, error) {
		if t != reflect.TypeOf(decimal.Decimal{}) {
			return data, nil
		}

		switch f.Kind() {
		case reflect.String:
			val, err := decimal.NewFromString(data.(string))
			if err != nil {
				return nil, err
			}
			return val, nil
		case reflect.Float64:
			return decimal.NewFromFloat(data.(float64)), nil
		case reflect.Int64:
			return decimal.NewFromInt(data.(int64)), nil
		default:
			return data, nil
		}
	}
}

func toBigIntHookFunc() mapstructure.DecodeHookFunc {
	return func(
		f reflect.Type,
		t reflect.Type,
		data interface{}) (interface{}, error) {

		if t != reflect.TypeOf(big.Int{}) {
			return data, nil
		}

		switch f.Kind() {
		case reflect.String:
			val, success := new(big.Int).SetString(data.(string), 10)
			if !success {
				return nil, errors.New(fmt.Sprintf("failed to parse %s to big int", data.(string)))
			}
			return val, nil
		case reflect.Float64:
			return big.NewInt(int64(data.(float64))), nil
		case reflect.Int64:
			return big.NewInt(data.(int64)), nil
		default:
			return data, nil
		}
	}
}

// input type MUST be map[string]interface{}
func decode(input interface{}, result interface{}) error {
	decoder, err := mapstructure.NewDecoder(&mapstructure.DecoderConfig{
		Metadata: nil,
		DecodeHook: mapstructure.ComposeDecodeHookFunc(
			toTimeHookFunc(),
			toDecimalHookFunc(),
			toBigIntHookFunc(),
		),
		Result: result,
	})
	if err != nil {
		return err
	}
	return decoder.Decode(input)
}
