package golibrmsg

// RedisSubscriberHandler ...
type RedisSubscriberHandler interface {
	HandlerFunc(*RedisMessage)
	Close()
}
