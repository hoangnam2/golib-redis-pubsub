package golibrmsg

import (
	"gitlab.com/golibs-starter/golib"
	"go.uber.org/fx"
)

// RedisPublisherOpt ...
func RedisPublisherOpt() fx.Option {
	return fx.Options(
		golib.ProvideProps(NewRedisPublisherProperties),
		fx.Provide(NewRedisPublisher),
	)
}
