package golibrmsg

import (
	"context"
	"gitlab.com/golibs-starter/golib"
	"gitlab.com/golibs-starter/golib/web/log"
	"go.uber.org/fx"
)

// RedisSubscriberOpt ...
func RedisSubscriberOpt() fx.Option {
	return fx.Options(
		golib.ProvideProps(NewRedisSubscriberProperties),
		fx.Provide(
			fx.Annotate(
				NewRedisSubscriberManager,
				fx.ParamTags(`group:"redis_consumer_handler"`),
			),
		),
		fx.Invoke(StartRedisSubscriberManager),
		fx.Invoke(OnStopRedisSubscriberHook),
	)
}

// StartRedisSubscriberManager ...
func StartRedisSubscriberManager(ctx context.Context, consumerManager *RedisSubscriberManager) {
	go consumerManager.Start(ctx)
}

// OnStopRedisConsumerIn ...
type OnStopRedisConsumerIn struct {
	fx.In
	Lc                   fx.Lifecycle
	RedisConsumerManager *RedisSubscriberManager `optional:"true"`
}

// OnStopRedisSubscriberHook ...
func OnStopRedisSubscriberHook(in OnStopRedisConsumerIn) {
	in.Lc.Append(fx.Hook{
		OnStop: func(ctx context.Context) error {
			log.Infof("Receive stop signal for redis consumer")
			in.RedisConsumerManager.Stop()
			return nil
		},
	})
}

// ProvideRedisSubscriber ...
func ProvideRedisSubscriber(handler interface{}) fx.Option {
	return fx.Provide(fx.Annotated{Group: "redis_consumer_handler", Target: handler})
}
