package golibrmsg

import (
	"encoding/json"
	"fmt"
	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"
	"math/big"
	"testing"
	"time"
)

func TestRedisMessageDecoder(t *testing.T) {
	t.Run("decode time.Time", func(t *testing.T) {

		type TestFrom struct {
			Time          time.Time `json:"time" mapstructure:"time"`
			TimeAsString  string    `json:"time_as_string" mapstructure:"time_as_string"`
			TimeAsFloat64 float64   `json:"time_as_float64" mapstructure:"time_as_float64"`
			TimeAsInt64   int64     `json:"time_as_int64" mapstructure:"time_as_int64"`
		}

		type TestTo struct {
			Time          time.Time `json:"time" mapstructure:"time"`
			TimeAsString  time.Time `json:"time_as_string" mapstructure:"time_as_string"`
			TimeAsFloat64 time.Time `json:"time_as_float64" mapstructure:"time_as_float64"`
			TimeAsInt64   time.Time `json:"time_as_int64" mapstructure:"time_as_int64"`
		}

		now := time.Now()
		payload := TestFrom{
			Time:          now,
			TimeAsString:  now.Format(time.RFC3339Nano),
			TimeAsFloat64: float64(now.UnixMilli()),
			TimeAsInt64:   now.UnixMilli(),
		}
		msg, err := NewRedisMessage("test", "Test Decode", payload)
		assert.Nil(t, err)

		marshaledMsg, err := json.Marshal(msg)
		assert.Nil(t, err)

		var unmarshalledMsg RedisMessage
		assert.Nil(t, json.Unmarshal(marshaledMsg, &unmarshalledMsg))

		var resultFrom TestFrom
		assert.Nil(t, unmarshalledMsg.Decode(&resultFrom))
		assert.Equal(t, resultFrom.Time.Unix(), now.Unix())
		assert.Equal(t, resultFrom.TimeAsString, now.Format(time.RFC3339Nano))
		assert.Equal(t, resultFrom.TimeAsFloat64, float64(now.UnixMilli()))
		assert.Equal(t, resultFrom.TimeAsInt64, now.UnixMilli())

		var resultTo TestTo
		assert.Nil(t, unmarshalledMsg.Decode(&resultTo))
		assert.Equal(t, resultTo.Time.UnixMilli(), now.UnixMilli())
		assert.Equal(t, resultTo.TimeAsString.UnixMilli(), now.UnixMilli())
		assert.Equal(t, resultTo.TimeAsFloat64.UnixMilli(), now.UnixMilli())
		assert.Equal(t, resultTo.TimeAsInt64.UnixMilli(), now.UnixMilli())
	})

	t.Run("decode decimal.Decimal", func(t *testing.T) {

		type TestFrom struct {
			RawDecimal       decimal.Decimal `json:"raw_decimal" mapstructure:"raw_decimal"`
			DecimalAsString  string          `json:"decimal_as_string" mapstructure:"decimal_as_string"`
			DecimalAsFloat64 float64         `json:"decimal_as_float64" mapstructure:"decimal_as_float64"`
			DecimalAsInt64   int64           `json:"decimal_as_int64" mapstructure:"decimal_as_int64"`
		}

		type TestTo struct {
			RawDecimal       decimal.Decimal `json:"raw_decimal" mapstructure:"raw_decimal"`
			DecimalAsString  decimal.Decimal `json:"decimal_as_string" mapstructure:"decimal_as_string"`
			DecimalAsFloat64 decimal.Decimal `json:"decimal_as_float64" mapstructure:"decimal_as_float64"`
			DecimalAsInt64   decimal.Decimal `json:"decimal_as_int64" mapstructure:"decimal_as_int64"`
		}

		payload := TestFrom{
			RawDecimal:       decimal.NewFromFloat(100.1),
			DecimalAsString:  "101.99",
			DecimalAsFloat64: 102.78,
			DecimalAsInt64:   10389,
		}
		msg, err := NewRedisMessage("test", "Test Decode", payload)
		assert.Nil(t, err)

		marshaledMsg, err := json.Marshal(msg)
		assert.Nil(t, err)

		var unmarshalledMsg RedisMessage
		assert.Nil(t, json.Unmarshal(marshaledMsg, &unmarshalledMsg))

		var resultFrom TestFrom
		assert.Nil(t, unmarshalledMsg.Decode(&resultFrom))
		assert.Equal(t, resultFrom.RawDecimal, decimal.NewFromFloat(100.1))
		assert.Equal(t, resultFrom.DecimalAsString, "101.99")
		assert.Equal(t, resultFrom.DecimalAsFloat64, 102.78)
		assert.Equal(t, resultFrom.DecimalAsInt64, int64(10389))

		var resultTo TestTo
		assert.Nil(t, unmarshalledMsg.Decode(&resultTo))
		assert.Equal(t, resultTo.RawDecimal, decimal.NewFromFloat(100.1))
		assert.Equal(t, resultTo.DecimalAsString, decimal.NewFromFloat(101.99))
		assert.Equal(t, resultTo.DecimalAsFloat64, decimal.NewFromFloat(102.78))
		assert.Equal(t, resultTo.DecimalAsInt64, decimal.NewFromFloat(10389))
	})

	t.Run("decode big.Int", func(t *testing.T) {

		type TestFrom struct {
			RawBigInt      *big.Int `json:"raw_big_int" mapstructure:"raw_big_int"`
			BigIntAsString string   `json:"big_int_as_string" mapstructure:"big_int_as_string"`
			BigIntAsInt64  int64    `json:"big_int_as_int64" mapstructure:"big_int_as_int64"`
		}

		type TestTo struct {
			RawBigInt      *big.Int `json:"raw_big_int" mapstructure:"raw_big_int"`
			BigIntAsString *big.Int `json:"big_int_as_string" mapstructure:"big_int_as_string"`
			BigIntAsInt64  *big.Int `json:"big_int_as_int64" mapstructure:"big_int_as_int64"`
		}

		payload := TestFrom{
			RawBigInt:      big.NewInt(18901123),
			BigIntAsString: "99991132",
			BigIntAsInt64:  12387653,
		}
		msg, err := NewRedisMessage("test", "Test Decode", payload)
		assert.Nil(t, err)

		marshaledMsg, err := json.Marshal(msg)
		assert.Nil(t, err)

		var unmarshalledMsg RedisMessage
		assert.Nil(t, json.Unmarshal(marshaledMsg, &unmarshalledMsg))

		var resultFrom TestFrom
		assert.Nil(t, unmarshalledMsg.Decode(&resultFrom))
		fmt.Println(resultFrom)
		assert.Equal(t, resultFrom.RawBigInt, big.NewInt(18901123))
		assert.Equal(t, resultFrom.BigIntAsString, "99991132")
		assert.Equal(t, resultFrom.BigIntAsInt64, int64(12387653))

		var resultTo TestTo
		assert.Nil(t, unmarshalledMsg.Decode(&resultTo))
		assert.Equal(t, resultTo.RawBigInt, big.NewInt(18901123))
		assert.Equal(t, resultTo.BigIntAsString, big.NewInt(99991132))
		assert.Equal(t, resultTo.BigIntAsInt64, big.NewInt(12387653))
	})
}
