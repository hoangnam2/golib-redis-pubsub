# Golib Redis PubSub

Redis PubSub solutions for Golang project.

### Setup instruction

Base setup, see [GoLib Instruction](https://gitlab.com/golibs-starter/golib/-/blob/develop/README.md)

Both `go get` and `go mod` are supported.

```shell
go get gitlab.com/golibs-starter/golib-message-bus
```

### Usage

Using `fx.Option` to include dependencies for injection.

```go
package main

import (
	golibdata "gitlab.com/golibs-starter/golib-data"
)

func main() {
    fx.New(
        // Required
		golibdata.RedisOpt(),

        // When you want to produce message to Kafka.
        // Graceful shutdown enabled by default.
		redismsg.RedisPublisherOpt(),

        // When you want to consume message from Kafka.
		redismsg.RedisSubscriberOpt(),

        // When you want to register a subscriber.
        // Subscriber has to implement golibrmsg.RedisSubscriberHandler
		redismsg.ProvideRedisSubscriber(NewCustomSubscriber),


        // ==================== TEST UTILS =================

        // This useful when you want to pass messages directly to a subscriber during test.
        // This option needs to come with some configuration:
        // app.redis.subscriber:
        //      handlerMappings:
        //         TradeCreatedSubscriber:
        //             enable: false
        //
        // redismsg.HandleMessage("TradeCreatedSubscriber", []byte(message))
		redismsg.EnableRedisSubscriberTestUtil(),
    ).Run()
}

// CustomSubscriber is implementation of golibrmsg.RedisSubscriberHandler
type CustomSubscriber struct {
}

func NewCustomSubscriber() golibrmsg.RedisSubscriberHandler {
    return &CustomConsumer{}
}

func (c CustomSubscriber) HandlerFunc(message *golibrmsg.RedisMessage) {
    // Will run when a message arrived
}

func (c CustomSubscriber) Close() {
    // Will run when application stop
}
```

### Configuration

```yaml
app:
    redis:
        host: localhost
        port: 6379
        database: 0
        # Configuration for redismsg.RedisPublisherOpt()
        publisher:
            eventMappings:
              TradeCreated:
                    channel: c1.http-request # Defines the channel that event will be sent to.

        # Configuration for redismsg.RedisSubscriberOpt()
        subscriber:
            handlerMappings:
                TradeCreatedSubscriber:
                    channels: # When you want to subscribe to multiple channels
                        - c1.http.request-completed.test
                        - c1.order.order-created.test
                    enable: true
```
